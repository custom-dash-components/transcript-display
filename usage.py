import transcription_display
from dash import Dash, callback, html, Input, Output
import json

# Open the files with the test information
with open("test_segment_chunk_df.json", "r") as json_file:
    segment_chunks = json.load(json_file)
with open("test_segment_info_df.json", "r") as json_file:
    segment_info = json.load(json_file)

app = Dash(__name__)

app.layout = html.Div([
    transcription_display.TranscriptionDisplay(
        id='input',
        segment_info=segment_info,
        segment_chunks=segment_chunks
    ),
])

if __name__ == '__main__':
    app.run_server(debug=True)
