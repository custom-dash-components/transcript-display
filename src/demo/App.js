/* eslint no-magic-numbers: 0 */
import React, {Component} from 'react';

import { TranscriptionDisplay } from '../lib';

class App extends Component {

    constructor() {
        super();
        this.state = {
            value: '',
            segment_chunks: JSON.parse(`[
                {
                  "start_segment":136.0,
                  "end_segment":139.0,
                  "score":0.815963805
                },
                {
                  "start_segment":76.0,
                  "end_segment":79.0,
                  "score":0.812149167
                },
                {
                  "start_segment":72.0,
                  "end_segment":75.0,
                  "score":0.799829781
                },
                {
                  "start_segment":184.0,
                  "end_segment":187.0,
                  "score":0.795769155
                },
                {
                  "start_segment":36.0,
                  "end_segment":39.0,
                  "score":0.793621242
                },
                {
                  "start_segment":88.0,
                  "end_segment":91.0,
                  "score":0.792333364
                },
                {
                  "start_segment":140.0,
                  "end_segment":143.0,
                  "score":0.791106224
                },
                {
                  "start_segment":148.0,
                  "end_segment":151.0,
                  "score":0.783744216
                },
                {
                  "start_segment":180.0,
                  "end_segment":183.0,
                  "score":0.781802833
                },
                {
                  "start_segment":80.0,
                  "end_segment":83.0,
                  "score":0.77881074
                },
                {
                  "start_segment":144.0,
                  "end_segment":147.0,
                  "score":0.777964234
                },
                {
                  "start_segment":84.0,
                  "end_segment":87.0,
                  "score":0.772389472
                },
                {
                  "start_segment":156.0,
                  "end_segment":159.0,
                  "score":0.770485699
                },
                {
                  "start_segment":124.0,
                  "end_segment":127.0,
                  "score":0.768234
                },
                {
                  "start_segment":96.0,
                  "end_segment":99.0,
                  "score":0.766258419
                },
                {
                  "start_segment":104.0,
                  "end_segment":107.0,
                  "score":0.765751362
                },
                {
                  "start_segment":188.0,
                  "end_segment":191.0,
                  "score":0.76537
                },
                {
                  "start_segment":100.0,
                  "end_segment":103.0,
                  "score":0.764370918
                },
                {
                  "start_segment":92.0,
                  "end_segment":95.0,
                  "score":0.76243341
                },
                {
                  "start_segment":64.0,
                  "end_segment":67.0,
                  "score":0.761806548
                },
                {
                  "start_segment":0.0,
                  "end_segment":3.0,
                  "score":0.757195413
                },
                {
                  "start_segment":168.0,
                  "end_segment":171.0,
                  "score":0.753693581
                },
                {
                  "start_segment":108.0,
                  "end_segment":111.0,
                  "score":0.75269717
                },
                {
                  "start_segment":164.0,
                  "end_segment":167.0,
                  "score":0.752572358
                },
                {
                  "start_segment":68.0,
                  "end_segment":71.0,
                  "score":0.751395464
                },
                {
                  "start_segment":176.0,
                  "end_segment":179.0,
                  "score":0.751128256
                },
                {
                  "start_segment":152.0,
                  "end_segment":155.0,
                  "score":0.749762058
                },
                {
                  "start_segment":40.0,
                  "end_segment":43.0,
                  "score":0.745551109
                },
                {
                  "start_segment":52.0,
                  "end_segment":55.0,
                  "score":0.744515121
                },
                {
                  "start_segment":132.0,
                  "end_segment":135.0,
                  "score":0.742649376
                },
                {
                  "start_segment":120.0,
                  "end_segment":123.0,
                  "score":0.741641104
                },
                {
                  "start_segment":12.0,
                  "end_segment":15.0,
                  "score":0.739105582
                },
                {
                  "start_segment":48.0,
                  "end_segment":51.0,
                  "score":0.737709224
                },
                {
                  "start_segment":56.0,
                  "end_segment":59.0,
                  "score":0.734149337
                },
                {
                  "start_segment":200.0,
                  "end_segment":203.0,
                  "score":0.733926237
                },
                {
                  "start_segment":28.0,
                  "end_segment":31.0,
                  "score":0.733347535
                },
                {
                  "start_segment":236.0,
                  "end_segment":239.0,
                  "score":0.733008564
                },
                {
                  "start_segment":244.0,
                  "end_segment":247.0,
                  "score":0.732038379
                },
                {
                  "start_segment":240.0,
                  "end_segment":243.0,
                  "score":0.731772304
                },
                {
                  "start_segment":24.0,
                  "end_segment":27.0,
                  "score":0.729041696
                },
                {
                  "start_segment":224.0,
                  "end_segment":227.0,
                  "score":0.728241205
                },
                {
                  "start_segment":248.0,
                  "end_segment":251.0,
                  "score":0.726727843
                },
                {
                  "start_segment":216.0,
                  "end_segment":219.0,
                  "score":0.725396872
                },
                {
                  "start_segment":4.0,
                  "end_segment":7.0,
                  "score":0.724192202
                },
                {
                  "start_segment":32.0,
                  "end_segment":35.0,
                  "score":0.723294377
                },
                {
                  "start_segment":232.0,
                  "end_segment":235.0,
                  "score":0.722248852
                },
                {
                  "start_segment":228.0,
                  "end_segment":231.0,
                  "score":0.721842766
                },
                {
                  "start_segment":172.0,
                  "end_segment":175.0,
                  "score":0.720625103
                },
                {
                  "start_segment":192.0,
                  "end_segment":195.0,
                  "score":0.719636738
                },
                {
                  "start_segment":20.0,
                  "end_segment":23.0,
                  "score":0.718255
                },
                {
                  "start_segment":128.0,
                  "end_segment":131.0,
                  "score":0.715109646
                },
                {
                  "start_segment":212.0,
                  "end_segment":215.0,
                  "score":0.713949442
                },
                {
                  "start_segment":112.0,
                  "end_segment":115.0,
                  "score":0.712638676
                },
                {
                  "start_segment":160.0,
                  "end_segment":163.0,
                  "score":0.711106598
                },
                {
                  "start_segment":204.0,
                  "end_segment":207.0,
                  "score":0.707179546
                },
                {
                  "start_segment":16.0,
                  "end_segment":19.0,
                  "score":0.706862152
                },
                {
                  "start_segment":208.0,
                  "end_segment":211.0,
                  "score":0.704516828
                },
                {
                  "start_segment":196.0,
                  "end_segment":199.0,
                  "score":0.700968504
                },
                {
                  "start_segment":60.0,
                  "end_segment":63.0,
                  "score":0.690044343
                },
                {
                  "start_segment":8.0,
                  "end_segment":11.0,
                  "score":0.68905443
                },
                {
                  "start_segment":220.0,
                  "end_segment":223.0,
                  "score":0.6765697
                },
                {
                  "start_segment":116.0,
                  "end_segment":119.0,
                  "score":0.669053674
                },
                {
                  "start_segment":44.0,
                  "end_segment":47.0,
                  "score":0.666376948
                }
              ]`),
            segment_info: JSON.parse(`[
                {
                  "segment":0,
                  "seek":0.0,
                  "text":" Hey everyone, Anthony Fantano here,"
                },
                {
                  "segment":1,
                  "seek":2.56,
                  "text":" Internet's busiest music nerd,"
                },
                {
                  "segment":2,
                  "seek":4.48,
                  "text":" and it's time for review of the new"
                },
                {
                  "segment":3,
                  "seek":6.72,
                  "text":" Clipping Album Splendor and Misery."
                },
                {
                  "segment":4,
                  "seek":9.04,
                  "text":" Clipping is an experimental hip hop trio"
                },
                {
                  "segment":5,
                  "seek":11.36,
                  "text":" been following them for a while now,"
                },
                {
                  "segment":6,
                  "seek":12.96,
                  "text":" given the group a couple of positive reviews"
                },
                {
                  "segment":7,
                  "seek":15.68,
                  "text":" from what they have released thus far."
                },
                {
                  "segment":8,
                  "seek":17.52,
                  "text":" And this new album of theirs, highly anticipated for me,"
                },
                {
                  "segment":9,
                  "seek":21.12,
                  "text":" especially since their last record in my opinion was so great,"
                },
                {
                  "segment":10,
                  "seek":24.16,
                  "text":" their sub-pop records debut."
                },
                {
                  "segment":11,
                  "seek":26.4,
                  "text":" It was an album loaded with these technical,"
                },
                {
                  "segment":12,
                  "seek":28.72,
                  "text":" fast, wordy, and grimy rap verses"
                },
                {
                  "segment":13,
                  "seek":31.52,
                  "text":" backed up with these sample-dense instrumentals,"
                },
                {
                  "segment":14,
                  "seek":34.32,
                  "text":" and not like sampling chunks of music samples,"
                },
                {
                  "segment":15,
                  "seek":37.68,
                  "text":" like clipping was incorporating sounds of ball bearings"
                },
                {
                  "segment":16,
                  "seek":41.28,
                  "text":" and static and distortion,"
                },
                {
                  "segment":17,
                  "seek":43.52,
                  "text":" and even alarm clocks to build the music of this record."
                },
                {
                  "segment":18,
                  "seek":47.68,
                  "text":" Now, at least from my perspective,"
                },
                {
                  "segment":19,
                  "seek":49.28,
                  "text":" as a music band, it seemed like the trio's future was hanging"
                },
                {
                  "segment":20,
                  "seek":52.8,
                  "text":" in the balance as frontman W. Diggs"
                },
                {
                  "segment":21,
                  "seek":55.36,
                  "text":" had landed a major role in the Broadway smash Hamilton."
                },
                {
                  "segment":22,
                  "seek":60.56,
                  "text":" So I was thinking, would he go back to clipping?"
                },
                {
                  "segment":23,
                  "seek":62.96,
                  "text":" Would he continue to pursue a career in theater?"
                },
                {
                  "segment":24,
                  "seek":67.28,
                  "text":" But it turns out he did go back with clipping,"
                },
                {
                  "segment":25,
                  "seek":69.36,
                  "text":" not too long after, coming back from Hamilton."
                },
                {
                  "segment":26,
                  "seek":72.16,
                  "text":" They put out the very rough wild dirty and low-fi wriggle EP,"
                },
                {
                  "segment":27,
                  "seek":76.0,
                  "text":" clipping really being as direct into the point as possible,"
                },
                {
                  "segment":28,
                  "seek":78.96,
                  "text":" as they could on a lot of these tracks, which I liked them,"
                },
                {
                  "segment":29,
                  "seek":82.48,
                  "text":" but it did feel in a way a little beneath them,"
                },
                {
                  "segment":30,
                  "seek":84.64,
                  "text":" because clipping, couldn't they have got a little more conceptual"
                },
                {
                  "segment":31,
                  "seek":87.44,
                  "text":" with this thing ambitious?"
                },
                {
                  "segment":32,
                  "seek":88.96,
                  "text":" But now that I hear splendor in misery,"
                },
                {
                  "segment":33,
                  "seek":90.96,
                  "text":" I think what they did on a wriggle makes total sense,"
                },
                {
                  "segment":34,
                  "seek":93.6,
                  "text":" because this is kind of the flip side in a way."
                },
                {
                  "segment":35,
                  "seek":96.96,
                  "text":" This album is not really catchy at all in some respects,"
                },
                {
                  "segment":36,
                  "seek":101.84,
                  "text":" and it is hugely conceptual."
                },
                {
                  "segment":37,
                  "seek":104.48,
                  "text":" Really a story album in a lot of respects,"
                },
                {
                  "segment":38,
                  "seek":107.04,
                  "text":" about a space traveler,"
                },
                {
                  "segment":39,
                  "seek":108.64,
                  "text":" which I was not expecting at all,"
                },
                {
                  "segment":40,
                  "seek":110.32,
                  "text":" but really I didn't know what to expect from this record,"
                },
                {
                  "segment":41,
                  "seek":113.04,
                  "text":" especially when I heard the first single from it,"
                },
                {
                  "segment":42,
                  "seek":116.0,
                  "text":" Baby Don't Sleep, which has this very skeletal,"
                },
                {
                  "segment":43,
                  "seek":118.88,
                  "text":" abstract, instrumental,"
                },
                {
                  "segment":44,
                  "seek":120.48,
                  "text":" which reminds me of their debut mix tape,"
                },
                {
                  "segment":45,
                  "seek":123.04,
                  "text":" which I liked quite a bit,"
                },
                {
                  "segment":46,
                  "seek":124.16,
                  "text":" but this track seemed even harder to digest."
                },
                {
                  "segment":47,
                  "seek":126.48,
                  "text":" Plus, the track list to this thing was loaded with all these"
                },
                {
                  "segment":48,
                  "seek":129.04,
                  "text":" one-minute detours and interludes."
                },
                {
                  "segment":49,
                  "seek":131.36,
                  "text":" On the surface, they might look like filler,"
                },
                {
                  "segment":50,
                  "seek":133.28,
                  "text":" but the truth is, every single one of them"
                },
                {
                  "segment":51,
                  "seek":135.36,
                  "text":" plays fluidly into the very cohesive narrative of this record."
                },
                {
                  "segment":52,
                  "seek":139.76,
                  "text":" It's almost like Davide's time in Hamilton"
                },
                {
                  "segment":53,
                  "seek":141.92,
                  "text":" inspired him to come back to clipping,"
                },
                {
                  "segment":54,
                  "seek":144.4,
                  "text":" and do something that was very theatrical, very cinematic."
                },
                {
                  "segment":55,
                  "seek":149.44,
                  "text":" And it's a story that's told with incredible detail"
                },
                {
                  "segment":56,
                  "seek":151.92,
                  "text":" right from the opening track, The Breach,"
                },
                {
                  "segment":57,
                  "seek":154.64,
                  "text":" where digs is wrapping incredibly fast to set the story."
                },
                {
                  "segment":58,
                  "seek":158.56,
                  "text":" And it seems like your typical opening clipping track,"
                },
                {
                  "segment":59,
                  "seek":161.84,
                  "text":" there's a little drone or some noise in the background,"
                },
                {
                  "segment":60,
                  "seek":164.24,
                  "text":" it's really about the speed and intensity of Davide's verse,"
                },
                {
                  "segment":61,
                  "seek":167.6,
                  "text":" and then at the very end of it,"
                },
                {
                  "segment":62,
                  "seek":168.8,
                  "text":" you're met with all these shots of distortion"
                },
                {
                  "segment":63,
                  "seek":170.8,
                  "text":" and feedback, but it kind of sounds like there's a bit"
                },
                {
                  "segment":64,
                  "seek":173.92,
                  "text":" of a sci-fi twist to the sounds this time around."
                },
                {
                  "segment":65,
                  "seek":176.8,
                  "text":" And unfortunately, he does not end his verse with"
                },
                {
                  "segment":66,
                  "seek":179.84,
                  "text":" its clipping bitch, probably because it doesn't play"
                },
                {
                  "segment":67,
                  "seek":183.2,
                  "text":" into the narrative of this story."
                },
                {
                  "segment":68,
                  "seek":184.8,
                  "text":" Now, on the song All Black, we have Davide wrapping again,"
                },
                {
                  "segment":69,
                  "seek":187.84,
                  "text":" but this time over a very sporadic,"
                },
                {
                  "segment":70,
                  "seek":191.28,
                  "text":" kind of drony, instrumental,"
                },
                {
                  "segment":71,
                  "seek":193.52,
                  "text":" sounds like Jonathan Snipes and William Hudson"
                },
                {
                  "segment":72,
                  "seek":195.44,
                  "text":" are really in movie soundtrack mode on this track."
                },
                {
                  "segment":73,
                  "seek":198.32,
                  "text":" And at this point in the album, it is told to us"
                },
                {
                  "segment":74,
                  "seek":201.28,
                  "text":" that the spaceship that the story is set on"
                },
                {
                  "segment":75,
                  "seek":204.72,
                  "text":" is being commandeered by our protagonist,"
                },
                {
                  "segment":76,
                  "seek":207.84,
                  "text":" who is referred to as cargo two, three, three, one."
                },
                {
                  "segment":77,
                  "seek":211.36,
                  "text":" And this story is kind of about his travels on the ship,"
                },
                {
                  "segment":78,
                  "seek":214.56,
                  "text":" the fact that he experiences, I guess what you could call,"
                },
                {
                  "segment":79,
                  "seek":217.36,
                  "text":" space madness."
                },
                {
                  "segment":80,
                  "seek":219.6,
                  "text":" On this particular cut on the record, he finds solace"
                },
                {
                  "segment":81,
                  "seek":223.92,
                  "text":" in wrapping to himself, he asks the computer, he asks the ship,"
                },
                {
                  "segment":82,
                  "seek":227.6,
                  "text":" if it has any beats for him to wrap on."
                },
                {
                  "segment":83,
                  "seek":229.36,
                  "text":" So instead, he's kind of wrapping against the weird sci-fi"
                },
                {
                  "segment":84,
                  "seek":234.4,
                  "text":" esque sounds of the ship itself,"
                },
                {
                  "segment":85,
                  "seek":236.64,
                  "text":" which is, I guess, why the instrumental sounds so odd."
                },
                {
                  "segment":86,
                  "seek":239.6,
                  "text":" And this track is also kind of a mission statement."
                },
                {
                  "segment":87,
                  "seek":241.44,
                  "text":" It's a bit of a life affirming moment."
                },
                {
                  "segment":88,
                  "seek":243.04,
                  "text":" Cargo two, three, three, one,"
                },
                {
                  "segment":89,
                  "seek":245.44,
                  "text":" says that he's not going to succumb to the All Black everything."
                },
                {
                  "segment":90,
                  "seek":248.64,
                  "text":" And I kind of get the sense in the story that he's"
                },
                {
                  "segment":91,
                  "seek":250.48,
                  "text":" being chased, being searched for."
                },
                {
                  "segment":92,
                  "seek":252.88,
                  "text":" And at the end of the track, the ship seems to put out this message"
                },
                {
                  "segment":93,
                  "seek":255.92,
                  "text":" where it's telling people not to come search for them,"
                },
                {
                  "segment":94,
                  "seek":258.4,
                  "text":" not to come find them."
                },
                {
                  "segment":95,
                  "seek":259.76,
                  "text":" Cargo two, three, three, one is not a threat."
                },
                {
                  "segment":96,
                  "seek":262.32,
                  "text":" This is love, don't fuck with it."
                },
                {
                  "segment":97,
                  "seek":265.04,
                  "text":" So the ship might be in love with him, he might be in love with the ship."
                },
                {
                  "segment":98,
                  "seek":267.92,
                  "text":" Then we're hit with an interlude where I imagine it's kind of"
                },
                {
                  "segment":99,
                  "seek":270.8,
                  "text":" cargo two, three, three, one, wrapping over this static and distortion."
                },
                {
                  "segment":100,
                  "seek":275.2,
                  "text":" It kind of sounds like a loss transmission."
                },
                {
                  "segment":101,
                  "seek":278.08,
                  "text":" And this happens at a couple points on the record."
                },
                {
                  "segment":102,
                  "seek":280.16,
                  "text":" Our protagonist exploits on the ship, continue on the song, wake up,"
                },
                {
                  "segment":103,
                  "seek":284.32,
                  "text":" where he's kind of wrapping about going into hyper sleep."
                },
                {
                  "segment":104,
                  "seek":287.04,
                  "text":" It's pretty crazy because the the refrains are actually pretty frightening as"
                },
                {
                  "segment":105,
                  "seek":291.12,
                  "text":" David is wrapping."
                },
                {
                  "segment":106,
                  "seek":292.08,
                  "text":" There will be no here when you wake up."
                },
                {
                  "segment":107,
                  "seek":295.04,
                  "text":" There will be no here when you wake up."
                },
                {
                  "segment":108,
                  "seek":296.56,
                  "text":" I was like the very fast pace of the beat and how the"
                },
                {
                  "segment":109,
                  "seek":298.8,
                  "text":" kick drums were just"
                },
                {
                  "segment":110,
                  "seek":300.16,
                  "text":" and then all of a sudden things take a serious change of pace"
                },
                {
                  "segment":111,
                  "seek":305.2,
                  "text":" musically, right?"
                },
                {
                  "segment":112,
                  "seek":305.84,
                  "text":" With the song long way away, which sounds like a piece of old gospel,"
                },
                {
                  "segment":113,
                  "seek":311.6,
                  "text":" a spiritual, and this is performed by a very somber"
                },
                {
                  "segment":114,
                  "seek":315.28,
                  "text":" coral group with no real backing instrumentation of any kind."
                },
                {
                  "segment":115,
                  "seek":319.36,
                  "text":" And while it sounds nice at the time when I heard I'm like wow does this"
                },
                {
                  "segment":116,
                  "seek":323.36,
                  "text":" really fit into what is going on with the album so far?"
                },
                {
                  "segment":117,
                  "seek":327.04,
                  "text":" It's kind of confusing because sonically it sounds so much different than"
                },
                {
                  "segment":118,
                  "seek":330.56,
                  "text":" anything else."
                },
                {
                  "segment":119,
                  "seek":331.28,
                  "text":" But up until this point the album has been so unorthodox that sure why not throw"
                },
                {
                  "segment":120,
                  "seek":336.48,
                  "text":" a coral group piece in there, right?"
                },
                {
                  "segment":121,
                  "seek":338.8,
                  "text":" But as the album continues on there sort of seems to be this very clear juxtaposition"
                },
                {
                  "segment":122,
                  "seek":342.96,
                  "text":" of old and new."
                },
                {
                  "segment":123,
                  "seek":345.28,
                  "text":" Music that sounds like it had been written or recorded hundreds of years ago"
                },
                {
                  "segment":124,
                  "seek":349.28,
                  "text":" and then music that sounds like it's coming from at least a hundred years in the"
                },
                {
                  "segment":125,
                  "seek":352.64,
                  "text":" future."
                },
                {
                  "segment":126,
                  "seek":353.12,
                  "text":" And these two elements come together beautifully on the song True Believer"
                },
                {
                  "segment":127,
                  "seek":356.72,
                  "text":" where we have an interpolation of this slave spiritual into the chorus."
                },
                {
                  "segment":128,
                  "seek":361.2,
                  "text":" No one of me even I'm going home."
                },
                {
                  "segment":129,
                  "seek":363.12,
                  "text":" Which actually fit pretty snugly and nicely in between"
                },
                {
                  "segment":130,
                  "seek":365.76,
                  "text":" David's verses on the track. The song ends with like this robot box voice singing"
                },
                {
                  "segment":131,
                  "seek":371.92,
                  "text":" at the very finish which is pretty eerie but again fits into the track despite the"
                },
                {
                  "segment":132,
                  "seek":376.48,
                  "text":" anachronistic clash of sounds."
                },
                {
                  "segment":133,
                  "seek":378.48,
                  "text":" And it's on this track that a few things become clear that there are religious"
                },
                {
                  "segment":134,
                  "seek":381.92,
                  "text":" parallels between slave spirituals and David's lyrics."
                },
                {
                  "segment":135,
                  "seek":385.12,
                  "text":" But also his reference directly to slave ships makes it apparent that"
                },
                {
                  "segment":136,
                  "seek":389.28,
                  "text":" our protagonist here is a victim of what I guess is a futuristic"
                },
                {
                  "segment":137,
                  "seek":394.0,
                  "text":" intergalactic slave trade. He's literally a slave in the space age."
                },
                {
                  "segment":138,
                  "seek":398.8,
                  "text":" He's cargo. He's on a cargo slave ship in space."
                },
                {
                  "segment":139,
                  "seek":403.2,
                  "text":" The front cover is literally depicting that as from the bottom half"
                },
                {
                  "segment":140,
                  "seek":407.52,
                  "text":" he is in torn pants and shackles but from his legs up he's in an astronaut suit."
                },
                {
                  "segment":141,
                  "seek":413.76,
                  "text":" Which again I think plays into the fact that David had spent so much time"
                },
                {
                  "segment":142,
                  "seek":418.56,
                  "text":" in Hamilton because I mean the period of time in which he was playing his character."
                },
                {
                  "segment":143,
                  "seek":423.36,
                  "text":" Slavery? Pretty popular. Now the song air amount is the album's lone"
                },
                {
                  "segment":144,
                  "seek":428.32,
                  "text":" banger. It's the song that I think worked best as a single for this project"
                },
                {
                  "segment":145,
                  "seek":432.8,
                  "text":" though still. I think it makes more sense within the context of the album"
                },
                {
                  "segment":146,
                  "seek":436.72,
                  "text":" than it makes outside of the context of the album. Because while it is loaded with"
                },
                {
                  "segment":147,
                  "seek":440.32,
                  "text":" some clever bragadocious lines a lot of them reference space and space travel"
                },
                {
                  "segment":148,
                  "seek":446.16,
                  "text":" sci-fi shit. Also one line about the protagonist haters or his"
                },
                {
                  "segment":149,
                  "seek":452.08,
                  "text":" enemies having had to make the noose a little tighter. I also like how the"
                },
                {
                  "segment":150,
                  "seek":455.6,
                  "text":" ending of the track blossoms with these beautiful glossy"
                },
                {
                  "segment":151,
                  "seek":458.8,
                  "text":" eerie synthesizers. And the ending the last several tracks of this record"
                },
                {
                  "segment":152,
                  "seek":462.88,
                  "text":" are pretty intense and add to the story quite a bit. We have the song"
                },
                {
                  "segment":153,
                  "seek":465.68,
                  "text":" Break the Glass where cargo two three three one pretty much goes crazy"
                },
                {
                  "segment":154,
                  "seek":470.88,
                  "text":" in a way loses communication with the ship. So the intensity of the"
                },
                {
                  "segment":155,
                  "seek":474.96,
                  "text":" instrumental really ramps up gets noisy. Sirens going off, tons of texture"
                },
                {
                  "segment":156,
                  "seek":479.68,
                  "text":" and this just frightening refrain where he is wrapping about having to"
                },
                {
                  "segment":157,
                  "seek":483.92,
                  "text":" break the glass in order to either get attention, get out of the ship, or just"
                },
                {
                  "segment":158,
                  "seek":488.96,
                  "text":" feel something. And then from here we have a bit of an"
                },
                {
                  "segment":159,
                  "seek":492.08,
                  "text":" interlude on the song Story 5. Now you might notice that if you've listened to"
                },
                {
                  "segment":160,
                  "seek":496.88,
                  "text":" everything Clipping has done up until this point we've only had two"
                },
                {
                  "segment":161,
                  "seek":499.92,
                  "text":" installments of the story series so far. So with this album being set into the"
                },
                {
                  "segment":162,
                  "seek":504.4,
                  "text":" future now in the story series we are also going into the future"
                },
                {
                  "segment":163,
                  "seek":508.88,
                  "text":" I guess. I've heard that we will understand better what this track's"
                },
                {
                  "segment":164,
                  "seek":513.92,
                  "text":" connection is to the other two story tracks."
                },
                {
                  "segment":165,
                  "seek":516.56,
                  "text":" One Story 3 and Story 4 are released. But in this particular cut"
                },
                {
                  "segment":166,
                  "seek":520.96,
                  "text":" we are hearing another track that sounds like a spiritual,"
                },
                {
                  "segment":167,
                  "seek":523.92,
                  "text":" that sounds like a folk song. It's littered by a coral group"
                },
                {
                  "segment":168,
                  "seek":527.36,
                  "text":" and it is about grace, a woman named Grace who is going overseas,"
                },
                {
                  "segment":169,
                  "seek":532.56,
                  "text":" fighting battles. The story doesn't necessarily tie into the overall theme and"
                },
                {
                  "segment":170,
                  "seek":537.2,
                  "text":" concept of the record but there are some parallels on this track"
                },
                {
                  "segment":171,
                  "seek":541.2,
                  "text":" I think with being out of your element, being away from home."
                },
                {
                  "segment":172,
                  "seek":546.48,
                  "text":" I think we're basically supposed to relate Grace's struggles on this track to"
                },
                {
                  "segment":173,
                  "seek":549.76,
                  "text":" the struggles of the protagonist and just kind of use this track again"
                },
                {
                  "segment":174,
                  "seek":553.68,
                  "text":" as a folk song to just kind of help us make sense of the trials and"
                },
                {
                  "segment":175,
                  "seek":557.2,
                  "text":" tribulations that our protagonist is going through. Now in the song Baby Don't"
                },
                {
                  "segment":176,
                  "seek":560.88,
                  "text":" Sleep it sounds like we are getting the ship wrapping again or at least"
                },
                {
                  "segment":177,
                  "seek":564.32,
                  "text":" W.D. is wrapping from the standpoint of maybe the narrator that he was at"
                },
                {
                  "segment":178,
                  "seek":568.24,
                  "text":" the beginning of the record and the track is basically about our"
                },
                {
                  "segment":179,
                  "seek":571.6,
                  "text":" protagonist in same behavior on the ship as he just kind of"
                },
                {
                  "segment":180,
                  "seek":575.52,
                  "text":" falls further and further into madness due to his isolation."
                },
                {
                  "segment":181,
                  "seek":579.44,
                  "text":" Talking about how he is most likely not going to survive the cold"
                },
                {
                  "segment":182,
                  "seek":583.04,
                  "text":" grips of space. And right after this is the closing track it kind of sounds"
                },
                {
                  "segment":183,
                  "seek":587.36,
                  "text":" like we're leaving our protagonist in a better place as the"
                },
                {
                  "segment":184,
                  "seek":589.92,
                  "text":" instrumental is a little bit more triumphant and upbeat."
                },
                {
                  "segment":185,
                  "seek":593.2,
                  "text":" But still lyrically it's pretty much"
                },
                {
                  "segment":186,
                  "seek":598.0,
                  "text":" we're leaving this guy stranded in space and maybe he'll make it"
                },
                {
                  "segment":187,
                  "seek":602.48,
                  "text":" probably not. And the track gets almost trippy and surreal"
                },
                {
                  "segment":188,
                  "seek":606.16,
                  "text":" toward the back end with all these effects and some of the lyrics that"
                },
                {
                  "segment":189,
                  "seek":609.52,
                  "text":" that David is wrapping. It's kind of like clipping trying to make their own"
                },
                {
                  "segment":190,
                  "seek":614.0,
                  "text":" little finish to their 2001 space Odyssey. And that's really the record."
                },
                {
                  "segment":191,
                  "seek":618.64,
                  "text":" It's an interesting album and I know I really went into the concept of this"
                },
                {
                  "segment":192,
                  "seek":622.16,
                  "text":" thing but I like the music on this record a lot too. I love the"
                },
                {
                  "segment":193,
                  "seek":625.28,
                  "text":" instrumentals. Not just because I think they're super catchy. I mean"
                },
                {
                  "segment":194,
                  "seek":629.2,
                  "text":" that was a little underwhelming when I was first getting into the album because"
                },
                {
                  "segment":195,
                  "seek":632.64,
                  "text":" it was sort of confusing. But now that I understand the concept of the album"
                },
                {
                  "segment":196,
                  "seek":636.24,
                  "text":" so much more the sound of the instrumentals makes sense and is way more"
                },
                {
                  "segment":197,
                  "seek":640.64,
                  "text":" captivating to me. Now if there are some shortcomings of the album"
                },
                {
                  "segment":198,
                  "seek":644.4,
                  "text":" they are as follows. For one the moments when David is singing on this record"
                },
                {
                  "segment":199,
                  "seek":648.88,
                  "text":" aren't really that good. I wish he just kind of brought in some of the singers he had"
                },
                {
                  "segment":200,
                  "seek":653.12,
                  "text":" on the other tracks especially when he is singing on the closing song on this"
                },
                {
                  "segment":201,
                  "seek":657.44,
                  "text":" thing. To me the singing that he gives on this record is just not really up"
                },
                {
                  "segment":202,
                  "seek":662.4,
                  "text":" to par with his wrapping and feels little awkward at points."
                },
                {
                  "segment":203,
                  "seek":665.6,
                  "text":" And then there's the fact that a side from the song Airmout but even that"
                },
                {
                  "segment":204,
                  "seek":670.0,
                  "text":" track still. Nothing can be pulled out of the context of this"
                },
                {
                  "segment":205,
                  "seek":674.56,
                  "text":" record and really sound good or makes sense. You know there's not really"
                },
                {
                  "segment":206,
                  "seek":679.92,
                  "text":" anything that has single potential on this record so I think that's going to"
                },
                {
                  "segment":207,
                  "seek":682.88,
                  "text":" make it difficult for it to find an audience at least you know for your average"
                },
                {
                  "segment":208,
                  "seek":687.04,
                  "text":" person. The more patient listener the people have been following"
                },
                {
                  "segment":209,
                  "seek":690.0,
                  "text":" clipping up until this point. I think we'll find a lot to love"
                },
                {
                  "segment":210,
                  "seek":693.6,
                  "text":" on this record. It just doesn't really kind of have that"
                },
                {
                  "segment":211,
                  "seek":696.4,
                  "text":" banger potential that single potential that a lot of songs from their previous"
                },
                {
                  "segment":212,
                  "seek":699.92,
                  "text":" records did. So that can make it a little more difficult to swallow because"
                },
                {
                  "segment":213,
                  "seek":704.8,
                  "text":" you've got to listen to it as a holistic piece but"
                },
                {
                  "segment":214,
                  "seek":707.84,
                  "text":" clipping didn't really overstay their welcome with this thing because it is just"
                },
                {
                  "segment":215,
                  "seek":711.12,
                  "text":" 38 minutes so if you want to hear a tight well written"
                },
                {
                  "segment":216,
                  "seek":714.4,
                  "text":" detailed concept album and under 40 minutes clipping has delivered it."
                },
                {
                  "segment":217,
                  "seek":718.88,
                  "text":" Now that's really the extent of my complaints on this thing."
                },
                {
                  "segment":218,
                  "seek":721.52,
                  "text":" Aside from that there's a lot to love here. The lyrics, the story, the flow of the"
                },
                {
                  "segment":219,
                  "seek":726.64,
                  "text":" album, the combination of sounds and musical styles, old and new."
                },
                {
                  "segment":220,
                  "seek":730.88,
                  "text":" The fact that once again clipping has made a hip hop record but"
                },
                {
                  "segment":221,
                  "seek":734.96,
                  "text":" stylistically it's just so far outside of even the fringes of what you"
                },
                {
                  "segment":222,
                  "seek":741.92,
                  "text":" would normally categorize hip hop music to be. All the performances on this"
                },
                {
                  "segment":223,
                  "seek":745.68,
                  "text":" record are incredible I would say from the"
                },
                {
                  "segment":224,
                  "seek":748.8,
                  "text":" coral groups to the rap verses and finally it's again it's just worth"
                },
                {
                  "segment":225,
                  "seek":754.0,
                  "text":" praising that the trio has pulled together this really intriguing concept"
                },
                {
                  "segment":226,
                  "seek":758.96,
                  "text":" within such a short amount of time because there have been a lot of great"
                },
                {
                  "segment":227,
                  "seek":762.16,
                  "text":" concept records as of late but many of them have been an hour, been an hour"
                },
                {
                  "segment":228,
                  "seek":766.64,
                  "text":" and 30 minutes. So clipping in a way puts out really one of the most hard"
                },
                {
                  "segment":229,
                  "seek":770.8,
                  "text":" hitting and direct concept albums to come out in a while."
                },
                {
                  "segment":230,
                  "seek":773.52,
                  "text":" But all that being said I think this is really great record. I mean clipping"
                },
                {
                  "segment":231,
                  "seek":777.2,
                  "text":" has done it again you know I don't really love it as much as I do their last"
                },
                {
                  "segment":232,
                  "seek":782.24,
                  "text":" record or maybe I can't even really love it in the same way I do their last"
                },
                {
                  "segment":233,
                  "seek":785.12,
                  "text":" record because they're entirely different animals. I can always come back to"
                },
                {
                  "segment":234,
                  "seek":789.2,
                  "text":" that album for a ton of different tracks that are incredibly catchy,"
                },
                {
                  "segment":235,
                  "seek":792.08,
                  "text":" stand well on their own whereas with this record over here"
                },
                {
                  "segment":236,
                  "seek":795.44,
                  "text":" really have to listen to the whole thing in one sitting. I don't want to keep"
                },
                {
                  "segment":237,
                  "seek":799.52,
                  "text":" going I think I've said everything I need to say about this album."
                },
                {
                  "segment":238,
                  "seek":801.84,
                  "text":" I'm feeling strong to a light nine on this thing really impressed with it."
                },
                {
                  "segment":239,
                  "seek":805.52,
                  "text":" I hope you guys are as well and I hope I was able to shed some light"
                },
                {
                  "segment":240,
                  "seek":808.8,
                  "text":" onto what exactly is being said and the idea of this album or what I think the"
                },
                {
                  "segment":241,
                  "seek":814.08,
                  "text":" idea of this album is and I hope I didn't spoil too much of it for you"
                },
                {
                  "segment":242,
                  "seek":817.92,
                  "text":" either because I know that maybe if you miss some of this stuff going back and"
                },
                {
                  "segment":243,
                  "seek":821.52,
                  "text":" listening to it and looking for those elements looking for those"
                },
                {
                  "segment":244,
                  "seek":824.32,
                  "text":" hints looking for those parallels between the new the old"
                },
                {
                  "segment":245,
                  "seek":827.84,
                  "text":" slavery future past all that stuff could make the album more interesting"
                },
                {
                  "segment":246,
                  "seek":833.2,
                  "text":" certainly. Transition have you given this album a listen did you love it? Did you"
                },
                {
                  "segment":247,
                  "seek":839.2,
                  "text":" hate it? Would you rate it? You're the best you're the best social"
                },
                {
                  "segment":248,
                  "seek":841.68,
                  "text":" hire of you next hit the like if you like please subscribe and please don't cry"
                },
                {
                  "segment":249,
                  "seek":846.8,
                  "text":" if you disagree with my thoughts on this"
                },
                {
                  "segment":250,
                  "seek":849.12,
                  "text":" clipping album. Okay."
                },
                {
                  "segment":251,
                  "seek":852.48,
                  "text":" Anthony Fantano clipping splendor in misery forever."
                }
              ]`)
        };
        this.setProps = this.setProps.bind(this);
    }

    setProps(newProps) {
        this.setState(newProps);
    }

    render() {
        return (
            <div>
                <TranscriptionDisplay
                    setProps={this.setProps}
                    {...this.state}
                />
            </div>
        )
    }
}

export default App;
