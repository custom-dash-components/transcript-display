import React from 'react';
import PropTypes from 'prop-types';
import "./style.css"
import { scaleLinear, scaleLog } from 'd3-scale'

export default function TranscriptionDisplay(props) {

    // Determine the minimum and maximum relevancy scores for the segment chunks
    var min_score = 9007199254740991
    var max_score = 0
    var segment_score_mapping = {}
    props.segment_chunks.forEach((segment_chunk) => {
        var segment_score = Math.pow(segment_chunk.score, 2)
        if (segment_score < min_score) {
            min_score = segment_score
        }
        if (segment_score > max_score) {
            max_score = segment_score
        }

        // We're also going to create a mapping of segment --> score from the chunks
        for (let i of Array.from({ length: (Math.floor(segment_chunk.end_segment) - Math.floor(segment_chunk.start_segment)) + 1 }, (_, i) => i + Math.floor(segment_chunk.start_segment))) {
            segment_score_mapping[i] = segment_score
        }
    })
    var score_midpoint = max_score - ((max_score - min_score) / 1)

    // Now, we're going to define the color scale using D3-scale
    // define the color scale
    const colorScale = scaleLinear()
        .domain([min_score, max_score])
        .range([0, 1])

    const colorScaleLog = scaleLog()
    .domain([min_score, max_score])
    .range([0, 1])

    


    return (
        <div>
            {props.segment_info.map((segment, idx) => {
                console.log(`Linear: ${colorScale(segment_score_mapping[segment.segment])}`)
                console.log(`Log: ${colorScaleLog(segment_score_mapping[segment.segment])}`)
                return (
                    <div
                        key={`segment_${idx}`}
                        onClick={() => {
                            props.setProps({"seek": segment.seek})
                        }}
                        className="transcription-segment"
                        style={{ "display": "inline", "backgroundColor": `rgba(141, 235, 152, ${Math.pow(colorScaleLog(segment_score_mapping[segment.segment]), 4)})`}}>
                        {segment.text}
                    </div>
                )
            })}
        </div>
    )
}

TranscriptionDisplay.defaultProps = {};

TranscriptionDisplay.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    id: PropTypes.string,

    /**
     * A list of dictionaries containing information about the segments
     */
    segment_info: PropTypes.array,

    /**
     * A list of dictionaries containing information about the relevancy of the segment chunks
     */
    segment_chunks: PropTypes.array,

    /**
     * The current "seek" of the video 
     */
    seek: PropTypes.number,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
