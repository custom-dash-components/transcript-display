# AUTO GENERATED FILE - DO NOT EDIT

export ''_transcriptiondisplay

"""
    ''_transcriptiondisplay(;kwargs...)

A TranscriptionDisplay component.

Keyword arguments:
- `id` (String; optional): The ID used to identify this component in Dash callbacks.
- `seek` (Real; optional): The current "seek" of the video
- `segment_chunks` (Array; optional): A list of dictionaries containing information about the relevancy of the segment chunks
- `segment_info` (Array; optional): A list of dictionaries containing information about the segments
"""
function ''_transcriptiondisplay(; kwargs...)
        available_props = Symbol[:id, :seek, :segment_chunks, :segment_info]
        wild_props = Symbol[]
        return Component("''_transcriptiondisplay", "TranscriptionDisplay", "transcription_display", available_props, wild_props; kwargs...)
end

