webpackHotUpdatetranscription_display("main",{

/***/ "./src/lib/components/TranscriptionDisplay.react.js":
/*!**********************************************************!*\
  !*** ./src/lib/components/TranscriptionDisplay.react.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TranscriptionDisplay; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.css */ "./src/lib/components/style.css");
/* harmony import */ var _style_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var d3_scale__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! d3-scale */ "./node_modules/d3-scale/src/index.js");




function TranscriptionDisplay(props) {
  // Determine the minimum and maximum relevancy scores for the segment chunks
  var min_score = 100;
  var max_score = 0;
  var segment_score_mapping = {};
  props.segment_chunks.forEach(function (segment_chunk) {
    var segment_score = Math.pow(segment_chunk.score, 5);
    if (segment_score < min_score) {
      min_score = segment_score;
    }
    if (segment_score > max_score) {
      max_score = segment_score;
    }

    // We're also going to create a mapping of segment --> score from the chunks
    for (var _i = 0, _Array$from = Array.from({
        length: Math.floor(segment_chunk.end_segment) - Math.floor(segment_chunk.start_segment) + 1
      }, function (_, i) {
        return i + Math.floor(segment_chunk.start_segment);
      }); _i < _Array$from.length; _i++) {
      var i = _Array$from[_i];
      segment_score_mapping[i] = segment_score;
    }
  });
  var score_midpoint = max_score - (max_score - min_score) / 1;

  // Now, we're going to define the color scale using D3-scale
  // define the color scale
  var colorScale = Object(d3_scale__WEBPACK_IMPORTED_MODULE_3__["scaleLog"])().domain([min_score, max_score]).range(['rgba(0, 0, 0, 0)', 'rgba(141, 235, 152, 1)']);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, props.segment_info.map(function (segment, idx) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: "segment_".concat(idx),
      onClick: function onClick() {
        console.log(segment.seek);
      },
      className: "transcription-segment",
      style: {
        "display": "inline",
        "backgroundColor": "".concat(colorScale(segment_score_mapping[segment.segment]))
      }
    }, segment.text);
  }));
}
TranscriptionDisplay.defaultProps = {};
TranscriptionDisplay.propTypes = {
  /**
   * The ID used to identify this component in Dash callbacks.
   */
  id: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  /**
   * A list of dictionaries containing information about the segments
   */
  segment_info: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  /**
   * A list of dictionaries containing information about the relevancy of the segment chunks
   */
  segment_chunks: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  /**
   * Dash-assigned callback that should be called to report property changes
   * to Dash, to make them available for callbacks.
   */
  setProps: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
};

/***/ })

})
//# sourceMappingURL=fd0f477-main-wps-hmr.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmZDBmNDc3LW1haW4td3BzLWhtci5qcyIsInNvdXJjZVJvb3QiOiIifQ==