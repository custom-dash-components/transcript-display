# AUTO GENERATED FILE - DO NOT EDIT

from dash.development.base_component import Component, _explicitize_args


class TranscriptionDisplay(Component):
    """A TranscriptionDisplay component.


Keyword arguments:

- id (string; optional):
    The ID used to identify this component in Dash callbacks.

- seek (number; optional):
    The current \"seek\" of the video.

- segment_chunks (list; optional):
    A list of dictionaries containing information about the relevancy
    of the segment chunks.

- segment_info (list; optional):
    A list of dictionaries containing information about the segments."""
    _children_props = []
    _base_nodes = ['children']
    _namespace = 'transcription_display'
    _type = 'TranscriptionDisplay'
    @_explicitize_args
    def __init__(self, id=Component.UNDEFINED, segment_info=Component.UNDEFINED, segment_chunks=Component.UNDEFINED, seek=Component.UNDEFINED, **kwargs):
        self._prop_names = ['id', 'seek', 'segment_chunks', 'segment_info']
        self._valid_wildcard_attributes =            []
        self.available_properties = ['id', 'seek', 'segment_chunks', 'segment_info']
        self.available_wildcard_properties =            []
        _explicit_args = kwargs.pop('_explicit_args')
        _locals = locals()
        _locals.update(kwargs)  # For wildcard attrs and excess named props
        args = {k: _locals[k] for k in _explicit_args}

        super(TranscriptionDisplay, self).__init__(**args)
